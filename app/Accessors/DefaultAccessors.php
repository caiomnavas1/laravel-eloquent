<?php

namespace App\Accessors;

trait DefaultAccessors
{
    public function getTitleAttribute($value)
    {
        return strtoupper($value);
    }

    public function getTitleAndContentAttribute($value)
    {
        return $this->title.' - '.$this->content;
    }
}
