<?php

use Illuminate\Support\Facades\Route;
use \App\Models\User;
use \App\Models\Post;
use \Illuminate\Support\Str;
use \Illuminate\Http\Request;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/select', function (){
    //$users = User::all();
    //$users = User::where('id', 1)->get();
    //$user = User::where('id', 1)->first();
    //$user = User::first();
    //$user = User::find(10);
    //$user = User::findOrFail(request('id'));
    //$user = User::where('name', request('name'))->firstOrFail();
    $user = User::firstWhere('name', request('name'));

    dd($user);
});

Route::get('/where', function (User $user){
    $filter = 'ab';

    //$users = $user->get();
    //$users = $user->where('email', '=', 'sepulveda.tabata@example.org')->first();
    //$users = $user->where('email', 'sepulveda.tabata@example.org')->first();
    //$users = $user->where('name', 'LIKE', "%{$filter}%")->get();
    //$users = $user->where('name', 'LIKE', "%{$filter}%")->orWhere('name', "Carlos")->get(); | whereNot, whereIn, whereNotIn
    $users = $user->where('name', 'LIKE', "%{$filter}%")
                    ->orWhere(function ($query) use ($filter) {
                        $query->where('name', '<>', 'Carlos');
                        $query->where('name', '=', $filter);
                    })
                    ->toSql();


    dd($users);
});

Route::get('/pagination', function (){
    $filter = request('filter');
    $totalPage = request('paginate', 10);
    $users = User::where('name', 'LIKE', "%{$filter}%")->paginate($totalPage);
    return $users;
});

Route::get('/orderby', function (){
    $users = User::orderBy('name')->get();

    return $users;
});

Route::get('/insert', function (Post $post){

    $post->user_id = 1;
    $post->title   = 'Primeiro Post '.Str::random(10);
    $post->content = 'Conteúdo do Post';
    $post->date    = date('Y-m-d');

    $post->save();

    $posts = Post::orderBy('id', 'desc')->get();
    return $posts;
});

Route::get('/insert2', function (Request $request){

    /*$post = Post::create([
        'user_id' => 1,
        'title'   => 'Primeiro Post '.Str::random(10),
        'content' => 'Conteúdo do Post',
        'date'    => date('Y-m-d')
    ]);*/

    $post = Post::create($request->all());

    $posts = Post::orderBy('id', 'desc')->get();
    return $posts;
});

Route::get('/update', function (Request $request){
    $post = Post::find(1);

    /*$post->title = 'Título Atualizado';
    $post->save();*/

    $post->update($request->all());

    $posts = Post::get();
    return $posts;
});

Route::get('/delete', function (){
    $post = Post::where('id', 2)->first();

    /*$post->title = 'Título Atualizado';
    $post->save();*/

    //$post->destroy(1,2,3); - para vários registros
    $post->delete();

    $posts = Post::get();
    return $posts;
});

Route::get('/delete2', function (){
    Post::destroy(4);

    $posts = Post::get();
    return $posts;
});

Route::get('/accessor', function (){
    $post = Post::first();

    return $post;
});

Route::get('/mutators', function (){
    $user = User::first();
    $post = Post::create([
        'user_id' => $user->id,
        'title'   => 'Um novo título' . Str::random(10),
        'content' => Str::random(100),
        'date'    => now(),
    ]);

    return $post;
});

Route::get('/local-scope', function (){
    //$posts = Post::lastWeek()->get();
    //$posts = Post::today()->get();
    $posts = Post::between('2022-06-01', '2022-06-13')->get();

    return $posts;
});

Route::get('/anonymous-global-scope', function (){
    $posts = Post::withoutGlobalScope('year')->get();

    return $posts;
});

Route::get('/global-scope', function (){
    $posts = Post::withoutGlobalScope('year')->get();

    return $posts;
});

Route::get('/observer', function (){
    $post = Post::create([
        'user_id' => 5,
        'title'   => 'Um novo título' . Str::random(10),
        'content' => Str::random(100),
        'date'    => now(),
    ]);

    return $post;
});

Route::get('events', function (){
    $post = Post::create([
        'user_id' => 5,
        'title'   => 'Um novo título' . Str::random(10),
        'content' => Str::random(100),
        'date'    => now(),
    ]);

    return $post;
});
